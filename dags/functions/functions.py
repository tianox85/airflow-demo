from airflow.operators.python import get_current_context
import json
import requests


def set_response(response):
    res = response.json()
    status_code = response.status_code
    if status_code == 200:
        context = get_current_context()
        context["task_instance"].xcom_push(key="pokemon", value=res)
        return True

    return False

def use_request(**context):
    pokemon = context["task_instance"].xcom_pull(key="pokemon")
    print(pokemon)

def return_branch(**context):
    pokemon = context["task_instance"].xcom_pull(key="pokemon")
    print(pokemon['abilities'][1]['ability']['name'])
    if pokemon['abilities'][1]['ability']['name'] == 'lightning-rod':
        return "lightning"
    else:
        return "wind"

def consume_api(pokemon):
    print(pokemon)
    response = requests.get("https://pokeapi.co/api/v2/pokemon/" + pokemon)
    print(response.json())