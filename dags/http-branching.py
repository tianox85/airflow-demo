"""
DAG for testing and tutorial
"""
from datetime import timedelta

import airflow
from airflow import DAG
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator

from functions.functions import use_request, set_response, return_branch, consume_api

DEFAULT_ARGS = {
    "owner": "airflow",
    "start_date": airflow.utils.dates.days_ago(1),
    "retries": 2,
    "retry_delay": timedelta(seconds=10),
}

with DAG(
    "http-branching-demo",
    default_args=DEFAULT_ARGS,
    schedule_interval='@hourly',
    tags=["tests"]
) as dag:
    http_simple = SimpleHttpOperator(
        task_id="http_simple",
        http_conn_id="pokemon",
        endpoint="pikachu",
        method="GET",
        # we could use jinja template
        # data=json.dumps({"priority": "{{ ti.xcom_pull(task_ids='id', key='my_key') }}"}),
        headers={"Content-Type": "application/json"},
        response_check=set_response,
        log_response=True
    )

    process_http_response = PythonOperator(
        task_id="process_http_response",
        python_callable=use_request,
        provide_context=True,
        trigger_rule="all_success"
    )

    branching = BranchPythonOperator(
        task_id='branching',
        python_callable=return_branch,
        provide_context=True)

    lightning = DummyOperator(task_id='lightning')

    wind = DummyOperator(task_id='wind')

    last_http_python_operator = PythonOperator(
        task_id="last_http_python_operator",
        python_callable=consume_api,
        provide_context=True,
        op_kwargs=dict(pokemon='ditto')
    )

    # pylint: disable=unused-argument,pointless-statement
    http_simple >> process_http_response >> branching >> [lightning, wind]
    lightning.set_downstream(last_http_python_operator)